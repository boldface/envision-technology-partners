<?php

/**
 * @package Boldface/Envision
 */
declare( strict_types = 1 );
namespace Boldface\Envision;

/**
 * Add updater controller to the init hook
 *
 * @since 0.1
 */
function init() {
  require __DIR__ . '/src/updater.php';
  $updater = new updater();
  $updater->init();
}
\add_action( 'init', __NAMESPACE__ . '\init' );

/**
 * Filter the array of body classes.
 *
 * @since 0.1.1
 *
 * @param array $class The original array of classes.
 *
 * @return array The modified array of classes.
 */
function bodyClass( array $class ) : array {
  $class[] = 'bg-light';
  return $class;
}
\add_filter( 'body_class', __NAMESPACE__ . '\bodyClass' );

/**
 * Filter the string of classes.
 *
 * @since 0.1.1
 *
 * @param string $class The original string of classes.
 *
 * @return string The modified string of classes.
 */
function entryClass( string $class ) : string {
  return trim( $class ) . ' bg-white' ;
}
\add_filter( 'Boldface\Bootstrap\Views\entry\class', __NAMESPACE__ . '\entryClass' );

/**
 * Filter the array of modules.
 *
 * @since 0.1
 *
 * @param array $modules The original array of modules.
 *
 * @return array The modified array of modules.
 */
function modules( array $modules ) : array {
  $modules[] = 'googleFonts';
  $modules[] = 'widgets';
  return $modules;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\modules' );

/**
 * Filter the array of widgets.
 *
 * @since 0.1.3
 *
 * @param array $modules The original array of widgets.
 *
 * @return array The modified array of widgets.
 */
function widgets( array $widgets ) : array {
  $widgets[] = [
    'name' => 'Footer',
    'id' => 'footer',
    'description' => 'Footer widgets',
    'before_widget' => '<div class="widget footer-widget %2$s">',
    'after_widget' => '</div>',
  ];
  return $widgets;
}
\add_filter( 'Boldface\Bootstrap\Models\widgets\sidebars', __NAMESPACE__ . '\widgets' );

/**
 * Filter the array of Google fonts.
 *
 * @since 0.1
 *
 * @param array $fonts The original array of fonts.
 *
 * @return array The modified array of fonts.
 */
function googleFonts( array $fonts ) : array {
  return [ 'Ubuntu' ];
}
\add_filter( 'Boldface\Bootstrap\Models\googleFonts', __NAMESPACE__ . '\googleFonts' );

/**
 * Filters the footer text to add copyright information.
 *
 * @since 0.1
 *
 * @param string $text The original footer text.
 *
 * @return string The modified footer text.
 */
function footerText( string $text ) : string {
  return '<span class="text-muted">Copyright &copy ' . date( 'Y' ) . ' Envision Technology. All rights Reserved</span>';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerText' );

/**
 * Filters the footer text to add the footer widget on the front page.
 *
 * @since 0.1
 *
 * @param string $text The original footer text.
 *
 * @return string The modified footer text.
 */
function footerTextWidget( string $footerText ) : string {
  if( ! \is_active_sidebar( 'footer' ) || ! \is_front_page() ) {
    return $footerText;
  }
  ob_start();
  \dynamic_sidebar( 'footer' );
  $footerWidgets = ob_get_clean();
  $widgets = sprintf(
    '<div class="widgets footer-widget bg-light">%1$s</div>',
    $footerWidgets
  );
  return $widgets . $footerText;
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerTextWidget' );

/**
 * Filter the navigation class.
 *
 * @since 0.1
 *
 * @param string $class The original navigation class.
 *
 * @return string The modified navigation class.
 */
function navigationClass( string $class ) : string {
  return 'navbar navbar-expand-md navbar-light bg-light fixed-top container';
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\class', __NAMESPACE__ . '\navigationClass' );

/**
 * Filter the Contact Form 7 button class.
 *
 * @since 0.1
 *
 * @param string $text The original Contact Form 7 button class.
 *
 * @return string The modified Contact Form 7 button class.
 */
function contactForm7buttonClass( string $class ) : string {
  return 'btn btn-secondary';
}
\add_filter( 'Boldface\Bootstrap\Models\contactForm7\button\class', __NAMESPACE__ . '\contactForm7buttonClass' );

/**
 * Filter the footer class.
 *
 * @since 0.1
 *
 * @param string $class The original footer class.
 *
 * @return string The modified footer class.
 */
function footerClass( string $class ) : string {
  return trim( str_replace( 'fixed-bottom', '', $class ) );
}
\add_filter( 'Boldface\Bootstrap\Views\footer\class', __NAMESPACE__ . '\footerClass' );

/**
 * Enqueue the footer script.
 *
 * @since 0.1
 */
function enqueueScripts() {
  //\wp_enqueue_script( 'footer', \get_stylesheet_directory_uri() . '/assets/js/footer.js', [ 'jquery' ] );
}
\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueueScripts' );

/**
 * Don't use the Autorow feature.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\Controllers\entry\autorow', '__return_false' );

/**
 * Don't use the REST feature.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );

/**
 * Remove all the filters from the entry header. Prevents autorow on the entry heading.
 *
 * @since 0.1
 *
 * @param string $heading The entry header.
 *
 * @return string The unmodified entry header.
 */
function entryHeader( string $heading ) : string {
  remove_all_filters( 'Boldface\Bootstrap\Views\entry\header' );
  return $heading;
}
\add_filter( 'Boldface\Bootstrap\Views\entry\header', __NAMESPACE__ . '\entryHeader', 99 );
