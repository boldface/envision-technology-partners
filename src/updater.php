<?php

/**
 * @package Boldface/Envision
 */
declare( strict_types = 1 );
namespace Boldface\Envision;

/**
 * Class for updating the theme
 *
 * @since 0.1
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'envision-technology-partners';
  protected $repository = 'boldface/envision';
}
